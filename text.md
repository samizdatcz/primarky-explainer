## článek v RSCR pod ID 3558908

# Klikatá cesta do Bílého domu začíná dnes primárkami v Iowě. Jak přesně fungují a proč jsou tak důležité?

Prérijní stát amerického středozápadu počtem obyvatel (lehce přes tři miliony) srovnatelný s Albánií, rozlohou o něco větší než Česko se Slovenskem dohromady a celkovým ekonomickým výkonem podobný Maďarsku.

Právě tam se v noci na úterý našeho času začne ve školních tělocvičnách a společenských sálech rozhodovat o tom, kdo se na podzim stane prezidentem Spojených států amerických.

Jak "výbory z Iowy" vznikly? Jak tento pro Evropana exotický volební rituál funguje? A proč je (nejen) pro Ameriku tak důležitý?

### 1. Proč jsou primárky v Iowě tak sledované?

Pro každého uchazeče o post prezidenta Spojených států je to první ostrý test, kolik voličů dokáže mobilizovat.

Výsledky z Iowy jsou první "tvrdá data" o skutečných vyhlídkách kanidátů na Bílý dům. Jména vítězů se stávají silným impulsem pro veřejné mínění v celé zemi, ovlivnit mohou i klání v dalších státech. Kandidát, který dokáže mezi lidmi v pravou chvíli vzbudit dostatečně silný dojem, že má šanci vyhrát, má totiž skutečně velkou šanci vyhrát.

Vítězům z Iowy se automaticky dostane zesílené pozornosti médií; komentátoři, zbytek politické scény, sponzoři kampaní i samotní voliči je začnou brát opravdu vážně. Ještě víc se to projeví poté, co primárky absolvuje tradičně druhý stát v pořadí, New Hampshire (letos se to stane v úterý 9. února).

Barack Obama v roce 2008 nevstupoval do voleb jako favorit. V celonárodních průzkumech zaostával za Hillary Clintonovou o 20 a více procent. Veškerou pozornost a sílu své kampaně ale soustředil těsně před primárkami na Iowu, a podařilo se mu tam těsně vyhrát. Během několika dnů se dotáhl na svou rivalku na dohled i v ostatních státech (byť New Hampshire tehdy nominoval Clintonovou). Podobnou taktikou se do Bílého domu dostal v roce 1976 další demokrat, Jimmy Carter. 

<iframe src="https://docs.google.com/spreadsheets/d/1InJzUdR308ZLADqeDn5YY88UrEufyFUmOU0lvyNne-Q/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false" width="610" height="380"></iframe>

Tabulka, kterou [sestavil web Vox](http://www.vox.com/cards/iowa-caucus-2016-polls/iowa-caucuses-past-winners) ukazuje, že s jedinou výjimkou každý kandidát, který od roku 1980 získal v otevřené soutěži demorkatickou či republikánskou nominaci, zahájil své vítězné tažení úspěchem v Iowě, v New Hampshiru nebo v obou těchto státech.

Onu jednu výjimku tvoří rok 1992. Tehdy bylo hlasování v Iowě silně ovlivněné tím, že na prezidenta kandidoval i poulární místní senátor Tom Harkin, kterému dali Iowané hlasy bez ohledu na jeho nízké preference v ostatních státech unie.


### 2. Kdo má letos největší šanci vyhrát?

[Podle statistika Natea Silvera](http://projects.fivethirtyeight.com/election-2016/primary-forecast/iowa-republican/), který se proslavil neobyčejně přesnými prognózami výsledků v posledních a předposledních amerických volbách, jsou šance hlavních kandidátů následující:

#### Republikáni

![Největší naději na vítězství má Donald Trump](republikani.png)

#### Demokraté

![Hillary Clintonová vede s velkým náskokem](demokrate.png)

Předpovědi jsou založené na statistickém modelu, který bere v úvahu desítky průzkumů voličských preferencí, ale také například popularitu a význam veřejně známých osobností, které jednotlivým kandidátům vyjádřily podporu.

Sám autor prognózy Nate Silver ale [připouští](http://fivethirtyeight.com/features/how-we-are-forecasting-the-2016-presidential-primary-election/), že u primárek mohou být výsledky krajně nepřesné. Sympatie voličů v nich jsou jako tekuté písky, mohou se rychle přelít i pár hodin před samotným hlasováním.

Platí to především pro nepřehledné a nezvykle vyrovnané startovní pole republikánů. Kdo jsou a odkud přicházejí hlavní kandidáti si můžete poslechnout v následujících příspěvcích Českého rozhlasu: 

- [Donald Trump](trump.wav)
- [Ted Cruz](cruz.wav)
- [Ben Carson](carson.wav)
- [Jeb Bush](bush.wav)
- [John Kasich](kasich.wav)

U demokratů se soutěž zúžila na duel mezi hlavní favoritku voleb Hillary Clintonovou a jejím vyzyvatelem, senátorem z Vermontu Bernie Sandersem. Obě kampaně v Iowě běží naplno: po celém státě vybudovaly síť jakýchsi kluboven, z nichž každý den až do posledních hodin před primárkami vyrážejí příznivci každého z kandidátů, obcházejí sousedy a při osobních rozhovorech je přesvědčují, komu mají dát svůj hlas.

Zatímco v Iowě má podle všech průzkumů navrch bývalá první dáma (byť se její náskok rychle zmenšuje), v New Hampshire, který má kulturně i geograficky blízko k jeho domovskému Vermontu, je silnější Sanders. Po celých Spojených státech se mu daří získávat přízeň především mladých voličů. Jeho rozhlasovou vizitku si můžete také poslechnout: 

- [Bernie Sanders](sanders.wav)

### 3. Jak přesně primárky probíhají?

V pondělí prvního února večer se sejdou přívrženci každé ze dvou hlavních amerických politických stran v 1 682 (tolik je v Iowě volebních okrsků) tělocvičnách, kostelech, konferenčních místnostech, veřejných knihovnách, společnenských sálech a občas i v obýváku některého ze sousedů.

A začnou debatovat o tom, kdo bude za jejich stranu nejvhodnějším kandidátem na prezidenta.

Každý, kdo se chce do debaty a hlasování zapojit, musí být registrovaným členem strany. Zároveň se ale může registrovat na počkání přímo na místě. Nárok na to má každý občan, kterému bude v době prezidentské volby 18 let. 

Obvykle se do výborů na obou stranách zapojí něco přes sto tisíc lidí, tedy jen asi pětina ze všech, kteří by mohli. Pokud kandidát nemá žádného silného vyzývatele (jako například Obama u demokratů v roce 2012), primárky se nekonají.

Samotný proces výběru nejvhodnějšího kandidáta se u demokratů a republikánů liší. Zajímavější je u demokratů:

Na začátku schůze se rozdělí do skupinek podle toho, kterého kandidáta podporují. Jedna bývá vyhrazena pro nerozhodné. Členové skupinek potom mají zhruba půl hodiny na to, aby přesvědčili členy ostatních houfů, že se mají raději přidat k nim.

Potom se členové skupinek přepočítají. Kandidát, jehož skupinka má méně než 15 procent účastníků schůze, je vyřazen a jeho podporovatelé se mohou přidat k někomu jinému, spojit se s jiným vyřazeným kandidátem, s jehož podporovateli dají dohromady alespoň 15 procent, nebo odejít.

Jak to vypadá v praxi, ukazuje následující video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cnh-136QqO8?t=39s" frameborder="0" allowfullscreen></iframe>

Tento neobvyklý mechanismus má zásadní vliv na výsledky, protože kandidátům mohou pomoci i voliči, kteří je nemají ve svém seznamu na prvním místě, ale jsou ochotni o nich uvažovat jako o náhradnících, když jejich favorit neprojde. Každý volič má tedy teoreticky dva hlasy.

Tímto způsobem každý okrsek zvolí své delegáty na okresní, a ti potom na regionální a celostátní (Iowský) výbor Demokratické strany. Delegáti (volitelé) tam potom rozhodnou, koho vyšlo na celostátní stranickou konferenci, která na konci července prezidentského kandidáta formálně posvětí.

Delegáti jsou přitom voleni proporcionálně a jejich počet odpovídá počtu voličů v daném okrsku. Může se tak například stát, že jeden okrsek pošle na okresní výbor strany dva delegáty, kteří zvednu ruku pro Clintonovou a dva pro Sanderse.

Delegáti k tomu nejsou zavázáni jinak než svou ctí a teoreticky by si mohli svou volbu ještě rozmyslet. V praxi se to ale stává tak málo, že hned večer po okrskových demokratických schůzích strana sečte počty vybraných delegátů sečte, výsledky jsou považovány za definitivní a zbytek vícestupňového volebního procesu je už jen méně sledovanou formalitou.

Republikánské primárky navenek více připomínají tradiční volební akt: voliči odcházejí za plentu a vhazují hlasovací lístky do volební urny. 

Ani republikáni ovšem nevolí ve svých primárkách přímo prezidentské kandidáty, nýbrž delegáty, kteří pak rozhodují o kandidátech na okresní, regionální, státní a celonárodní úrovni.

Až do minulých voleb byl tento proces krajně nepřehledný, u mnoha delgátů nebylo jisté, pro kterého prezidentského kandidáta budou na vyšších úrovních nakonec hlasovat. Šlo tedy spíš o jakousi nezávaznou anketu než o skutečné volby; svědčí o tom třeba i fakt, že výsledky v osmi okrscích se od minulých primárek v roce 2012 dodnes nepodařilo sečíst.

Od letošního roku platí přísnější pravidla a republikánští delegáti mají povinnost již před primárkami jasně říct, pro kterého prezidentského kandidáta budou skutečně hlasovat – a pak se tímto závazkem také řídit. 

### 4. Proč je Iowa první?

Nebylo tomu tak vždy. Demokratická strana Iowy posunula termín svých primárních voleb na přelom ledna a února až v roce 1972. Do té doby byl klíčovým prvním státem v současnosti druhý New Hampshire.

Demokraté tehdy reformovali celý systém primárek, cílem bylo dát více rozhodovacích pravomocí do rukou řadovým členům – do té doby měli hlavní slovo při výběrů kandidáta šéfové stranických organizací. Podobnou změnu záhy poté uskutčnili i republikáni.

Důvod výběru tak časného termínu v Iowě je obestřen mýty a legendami: [jedna z nich vypráví](https://www.washingtonpost.com/blogs/ezra-klein/post/why-iowa-gets-to-go-first-and-other-facts-about-tonights-caucus/2011/08/25/gIQAJtygYP_blog.html) o tom, že v Des Moines, hlavním městě Iowy, nebyl v jiném termínu dostatek volných hotelových pokojů, [jiná o tom, že šlo](http://www.nytimes.com/1996/10/25/us/harold-hughes-iowa-trucker-turned-politician-dies-at-74.html) o účelový krok, který měl pomoci jednomu z místních kandidátů.

Celá populace Iowy tvoří jen necelé jedno procento obyvatel USA. Co do etnického složení se nejedná o reprezentativní vzorek obyvatel. Drtivá většina Iowanů jsou běloši ze střední třídy, často farmáři, protestanti a příslušníci americké nižší střední třídy. Demokratický volební proces navíc porušuje jeden ze základních demokratických principů: právo na tajnou volbu, bez nátlaku a hrozící ostrakizace od ostatních voličů. 

Kritici současného volebního systému na to často poukazují, ale není jim to mnoho platné. Demokratické ani republikánské elity se nedokážou shodnout na tom, který jiný stát by měl volit jako první. Když se o změnu volebního kalendáře v roce 1996 pokusila Lousiana, většina kandidátů to [prostě ignorovala](http://articles.chicagotribune.com/1996-01-28/news/9601280262_1_louisiana-republicans-caucuses-phil-gramm).  

###5. Jaké další překážky čekají zájemce na cestě do Bílého domu?

<div class="amara-embed" data-height="370px" data-width="620px" data-url="https://www.youtube.com/watch?v=lG5w4GeCZJY">
</div>

Maraton primárek začíná na začátku února v Iowě a New Hampshire, končí 14. června v hlavním městě Spojených států. Tou dobou už ale bude nejspíš o kandidátech obou hlavních stran rozhodnuto.

Klíčové bude především "Superúterý" 1. března, kdy půjdou k primárním volbám a volebním výborům registrovaní členové obou stran v patnácti státech včetně velkého a vlivného Texasu a významných států "střední váhové kategorie" Massachusetts, Georgie a Virginie.  

Počet delegátů nominovaých za každý stát se určuje podle vzorce, který bere v úvahu počet hlasů, jež daný stát odevzdal v předchozích volbách kandidátovi dané strany, a také počet zástupců toho kterého státu ve sboru volitelů, jenž formálně volí prezidenta – tedy de facto význam, jenž každému státu připisuje ústava Spojených států. 

Každý stát má pro výběr delegátů na celonárodní sjezdy svá vlastní, mírně odlišná pravidla. Volební schůze podobné těm z Iowy si zachovalo už jen osm z jedenapadesáti.   

Výsledkem primárek je seznam delegátů, kteří na stranických sjezdech oficiálně posvětí dva hlavní kandidáty. Demokratický se koná od 25. do 28. července ve Filadefiii, republikánský o týden dřív v Clevelandu.

Do hry může zasáhnout i silný nezávislý kandidát. Bývalý starosta New Yorku a vlivný podnikatel Michael Bloomberg již oznámil, že  kandidaturu zváží v případě, že za republikány bude nominován Donald Trump nebo Ted Cruz a za demokraty Bernie Sanders. Pokud by uspěl, byla by to prvotřídní senzace: Od roku 1852 totiž v amerických prezidentských volbách vítězí pouze kandidáti dvou hlavních stran.

Hned po sjezdech začíná horká fáze kampaně. Volebním dnem je vždy první úterý po prvním pondělí v listopadu prezidenstkého volebního roku. Letos tedy půjdou Američané k urnám 8. listopadu. 

Formálně jsou přitom volby dvoustupňové. To znamená, že voliči neodevzdávají hlas přímo pro kandidáta, ale pro delegáty do sboru volitelů, který pak prostou většinou rozhoduje o tom, kdo projde do Bílého domu.

Sbor má celkem 538 členů; počet volitelů za jednotlivé státy odpovídá zastoupení poslanců ve Sněmovně reprezentantů a v Senátu.

Existence sboru volitelů má historické kořeny. Zakladatelé USA nevěřili ve schopnost "prostého lidu" zvolit si prezidenta přímo a dali státům právo vybrat si své delegáty. Systém ale také zároveň zajišťuje podstatnější hlas státům unie s nízkým počtem obyvatel. V případě přímé volby (v sedmi nejlidnatějších státech žije 45 procent obyvatel USA) by totiž důležitost méně lidnatých států výrazně poklesla.

Volitelé z každého státu se scházejí v první pondělí po druhé prosincové středě (letos to bude 19. prosince), aby příslušnému kandidátovi odevzdali hlasy. Vítězným kandidátem se v každém státu stává ten, jenž od volitelů obdrží nejvyšší počet hlasů. Volitelé se při své volbě řídí hlasováním občanů, ačkoli v některých státech nejsou jejich preferencí ze zákona vázáni.

Vítěznému kandidátovi jsou pak v daném státě připsány všechny hlasy volitelů, kterými stát disponuje. Jedná se o většinový volební systém („vítěz bere vše“). Výjimku tvoří Nebraska a Maine, kde takový výsledek nemusí automaticky nastat v důsledku odlišné metody přerozdělování hlasů. Následně jsou do Washingtonu, D.C. odeslány oddělené lístky se jmény kandidáta na úřad prezidenta a na úřad viceprezidenta (proceduru volby upravuje XII. dodatek Ústavy).

V pátek 6. ledna 2017 sečte hlasy na společné schůzi obou komer Kongres. Předseda Senátu ex officio, tj. viceprezident, předčíta došlé hlasy volitelů z unijních států. Kandidát, jenž dosáhne většiny, tj. překročí hranici 270 z celkových 538 volitelů, se stává prezidentem.

Obvykle to bývá už jen formalita, ale celý proces se může zdramtizovat. O prezidentovi USA už minulosti rozhodl i hlas jednoho jediného volitele: V roce 1876 zvítězil nejmenším možným rozdílem republikán Rutherford Hayes. V roce 2000 činil rozdíl mezi Georgem W. Bushem a demokratem Alem Gorem pouhých pět volitelů. Volby měly dohru u Nejvyššího soudu, který nakonec rozhodl o sporných výsledcích v klíčové Floridě ve prospěch Bushe. Ten se stal prezidentem i přesto, že Gore získal celkově více hlasů amerických voličů.

Nejvyšší počet volitelů – 525 – získal v roce 1984 republikán Ronald Reagan. Jeho protikandidát Walter Mondale jich měl jen třináct.

V pátek 20. ledna 2017 čeká budoucího prezidenta Spojených států slavnostní přísaha a inaugurace.

<script type="text/javascript" src='https://amara.org/embedder-iframe'>
</script>